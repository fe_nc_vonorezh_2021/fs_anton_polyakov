/*jshint esversion: 6 */
function run() {
    let running = true;
    while (running) {
        running = guessNumber();
    }
}

function guessNumber() {
    let secNumber = Math.floor(Math.random() * 1001);
    console.log(secNumber);
    let count = 0;
    while (true) {
        count++;

        let valueNumber = prompt('Введите число');
        if (valueNumber == null) {
            return false;
        }

        if (isNaN(valueNumber)) {
            alert('Введите число!');
        } else if (valueNumber > secNumber) {
            alert('Искомое число меньше');
        } else if (valueNumber < secNumber) {
            alert('Искомое число больше');
        } else {
            return confirm(`Вы угадали!Количество попыток: ${count}. Начать заново?`);
        }
    }
}