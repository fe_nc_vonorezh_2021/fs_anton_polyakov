/*jshint esversion: 6 */
let word = window.prompt("Введите слово:");

if (word === word.split("").reverse().join("")) {
    alert(`${word} палиндром!`);
} else {
    alert(`${word} не палиндром!`);
}
