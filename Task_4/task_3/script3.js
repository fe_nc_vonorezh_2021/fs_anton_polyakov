/*jshint esversion: 6 */
function createArray(arraySize) {
    let arr = [];
    let i = 0;
    while (i < arraySize) {
        arr.push(Math.floor(Math.random() * 100));
        ++i;
    }
    console.log(arr);
    return arr;

}


function sortArray(arr, direction) {
    if (direction == 'asc') {
        arr.sort((a, b) => a - b);
    }
    if (direction == 'desc') {
        arr.sort((a, b) => b - a);
    }
    console.log(arr);
}

function sumSquaresOddElements(arr) {
    let sum = arr.reduce((sum, elem) => sum + ((elem % 2) ? elem ** 2 : 0), 0);
    console.log(sum);
}

const size = 4;
let arr = createArray(size);
sortArray(arr, 'asc');
sortArray(arr, 'desc');
sumSquaresOddElements(arr);
