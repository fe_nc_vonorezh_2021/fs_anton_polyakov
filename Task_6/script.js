function send (){
    let form = document.forms.form;
    localStorage.name = form.name.value;
    localStorage.surname = form.surname.value;
    localStorage.email_adress = form.email_adress.value;
    localStorage.phone_number = form.phone_number.value;
    localStorage.message = form.message.value;
    let invalidFields = "";
    let regexp = new RegExp(/^\+\d\(\d{3}\)\d{2}\-\d{2}\-\d{3}$/);
    if(isEmpty(form.name.value)){
        invalidFields += " " + form.name.name;
    }

    if(isEmpty(form.surname.value)){
        invalidFields += " " + form.surname.name;
    }

    if(isEmpty(form.email_adress.value)){
        invalidFields += " " + form.email_adress.name;
    }

    if(isEmpty(form.phone_number.value)){
        invalidFields += " " + form.phone_number.name;
    }
    else if(!form.phone_number.value.match(regexp)){
        invalidFields += " " + form.phone_number.name;
    }

    if(isEmpty(form.message.value)){
        invalidFields += " " + form.message.name;
    }
    if(!isEmpty(invalidFields)){
        alert("Поля " + invalidFields + " заполнены не верно, пожалуйста исправьте");
        return;
    }
    let cookies = document.cookie.split(';').map(function(a){
        return a.trim().split('=');
    })
    for(let i=0; i < cookies.length; i++) {
        if(cookies[i][0] == 'sent' && cookies[i][1] == 'true'){
            alert(form.name.value + " " + form.surname.value + ", ваше обращение обрабатывается!");
            return;

        }
    }

    document.cookie = "firstName="+ form.name.value;
    document.cookie = "lastName=" + form.surname.value;
    document.cookie = "sent=true";
}

function isEmpty(str){
  return str === "" || str === null || str === undefined;
}



  
