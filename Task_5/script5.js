/*jshint esversion: 6 */
const $display = document.getElementById('display');
let elementArray = [];
let calculated = false;

function press(arg) {
    let operations = ['+', '-', 'x²', '√', '*', '/'];
    function notOperation(arg) {
        return operations.indexOf(arg) === -1;
    }
    let currentResult = 0;
    function calculate() {
        var currentOperation = '';
        elementArray.forEach(element => {
            if (!(notOperation(element))) {
                currentOperation = element;    // 
                if (currentOperation === '√') {
                    currentResult = Math.sqrt(currentResult);
                }
            }
            else {
                var intElem = parseInt(element);
                if (currentOperation === '') {
                    currentResult = intElem;
                }
                else {
                    if (currentOperation === '+') {
                        currentResult = currentResult + intElem;
                    }
                    if (currentOperation === '-') {
                        currentResult = currentResult - intElem;

                    }
                    if (currentOperation === 'x²') {
                        currentResult = Math.pow(currentResult, intElem);
                    }

                    if (currentOperation === '*') {
                        currentResult = currentResult * intElem;
                    }

                    if (currentOperation === '/') {
                        currentResult = currentResult / intElem;
                    }
                }
            }
        });
        return currentResult;
    }

    if (calculated) {
        clearAll();
    }

    $display.value = $display.value + arg;

    if (arg != '=') {
        if (notOperation(arg)) {
            if ((notOperation(elementArray[elementArray.length - 1])) && (elementArray.length > 0)) {
                elementArray[elementArray.length - 1] += arg;
            }
            else {
                elementArray.push(arg);
            }
        }
        else {
            elementArray.push(arg);
        }
    }
    else {
        $display.value = $display.value + calculate();
        calculated = true;
    }
    console.log('elementArray', elementArray);


}

function clearAll() {
    console.log('clear all');
    $display.value = '';
    elementArray = [];
    calculated = false;
}
