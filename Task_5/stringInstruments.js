class StringInstrument{
    constructor(stringCount){
        this.count = stringCount;
    }

    get stringCount(){
        return this.count;
    }

    set stringCount(count){
        this.count = count;
    }

    play(){
        console.log('StringInstrument start plaing');
        for(var i = 0; i < this.count; i++){
            console.log('Din - Din');
        }
        console.log('StringInstrument finish plaing');
    }
}

class Bandjo extends StringInstrument{
    constructor(stringCount,resonatorDevice,Spirit){
        super(stringCount);
        this.resonator = resonatorDevice;
        this.cowboySpirit = Spirit;
    }

    get resonator(){
        return this.resonatorDevice;
    }

    set resonator(resonatorDevice){
        this.resonatorDevice = resonatorDevice;
    }

    get cowboySpirit(){
        return this.spirit;
    }

    set cowboySpirit(spirit){
        this.spirit = spirit;
    }
        
         play(){
             console.log('Bandjo start playing');
            for(var i = 0; i < this.stringCount; i++){
                console.log('Don - Don');
            }
             console.log(this.resonator + ' resonator so cool');
             console.log(this.cowboySpirit + ' bless you');
             console.log('Bandjo final playing');
         }
}

class Guitar extends StringInstrument{
    constructor(stringCount,hole){
        super(stringCount);
        this.holeSize = hole;
    }

    get holeSize(){
        return this.hole;
    } 

    set holeSize(hole){
        return this.hole = hole;
    }

    play(){
        console.log('Guiatar start playing');
        for(var i = 0; i < this.stringCount;i++){
         console.log('Dan - Dan');
        }
        console.log('Hole size is' + this.hole);
        console.log('Guitar finish playing');
    }    
}

    

class ElectricGuitar extends Guitar{
    constructor(stringCount,cntrls){
        super(stringCount,0);
        this.controls = cntrls;
    }
    
    get controls(){
        return this.cntrls;
    }

    set controls(cntrls){
       return this.cntrls = cntrls;
    }

    play(){
        console.log('ElectricGuitar start playing');
        for(var i = 0; i < this.stringCount;i++){
            console.log('Dji - Dji');
        }
            console.log('The controls is ' + this.controls);
            console.log('ElectricGuitar final playing');      
    }

}
class TremoloGuitar extends ElectricGuitar{
    constructor(stringCount,trml){
        super(stringCount,0);
        this.tremolo = trml;
    }
    
    get tremolo(){
        return this.trml;
    }

    set tremolo(trml){
        return this.trml = trml;
    }

    play(){
        console.log('TremoloGuitar start playing');
        for(var i = 0; i < this.stringCount;i++){
            console.log('Wiu - Wiu');
        }
            console.log('Band up ' + this.tremolo);      
            console.log('TremoloGuitar final playing')    
    }

}

let instrument = new StringInstrument(5);



let guitar = new Guitar(6,'5cm');
guitar.stringCount = 7;
guitar.play(); 

let bandjo = new Bandjo(4,'big','Joe');
bandjo.play();

let controls = ['Volume', 'Tone', 'Switch'];
let electricguitar = new ElectricGuitar(8,controls);
electricguitar.play();

let tremologuitar = new TremoloGuitar(1,'tremolo');
tremologuitar.play(); 
